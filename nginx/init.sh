#!/bin/bash

# Esperar a que MySQL esté listo
while ! mysqladmin ping -hmysql-joknet --silent; do
  sleep 1
done

# Consultar MySQL y escribir el resultado en un archivo
mysql -h mysql-joknet -u admin-joknet -padmin-joknet mysql-joknet -e "SELECT * FROM jokTabla;" > /usr/share/nginx/html/mysql_data.txt

# Iniciar NGINX en modo daemon, esto es para que una vez que ocurra el script no se detenga el contenedor.
nginx -g 'daemon off;'
